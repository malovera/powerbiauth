from django.conf.urls import url
from django.conf import settings
from django.contrib.auth import logout
from django.conf.urls.static import static

from . import views

from .models import *
# ./ : ambito actual

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^dashboard$', views.dashboard, name='dashboard'),
	url(r'^login_submit_ajax$', views.login_submit_ajax, name='login_submit_ajax'),
]