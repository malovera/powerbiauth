$('.btn-entrar').click(function(){
	var validar = validar_inputs_inicio_sesion();

	if(validar)
	{
		var formData = new FormData($('#form-usuario')[0]);
		formData.append('csrfmiddlewaretoken', $('meta[name="_token"]').attr('content'));

		$.ajax({
			url:'login_submit_ajax',
			type:'post',
			data:formData,
			processData: false,  // tell jQuery not to process the data
	  		contentType: false,
			success: function(data){
				console.log(data);
				if(data=='1')
				{
					$('.div-loader').fadeIn();
					//Correcto
					location.replace("/dashboard");
				}
				else
				{
					//Incorrecto
					alert('Usuario o contraseña incorrecta');
				}
			}
		})
	}
	else
	{
		alert('Llene todos los campos para iniciar sesión');
	}
})

function validar_inputs_inicio_sesion()
{
	var respuesta = false;

	var input_usuario = $('.input-usuario');
	var input_password = $('.input-password');

	if(input_usuario.val().trim() != "" && input_password.val().trim() != "")
	{
		respuesta = true;
	}

	return respuesta;
}