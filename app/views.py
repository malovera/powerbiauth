from django.shortcuts import render

from django.contrib.auth.decorators import login_required

from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group

import requests, json

# Create your views here.
def index(request):
    try:
        logout(request)
        template = loader.get_template('index.html')
        
        context = {
            #'layers': layers
        }
        return HttpResponse(template.render(context, request))
    except IntegrityError:
        return HttpResponse("ERROR: already exists!")

@login_required
def dashboard(request):
    try:
        template = loader.get_template('dashboard.html')

        token = get_token()

        groups = get_groups({}, {'Authorization': 'Bearer ' + token['access_token']})

        group_id = groups['value'][0]['id']

        report = get_report({}, {'Authorization': 'Bearer ' + token['access_token']})

        report_id = report['value'][2]['id']
        report_embed_url = report['value'][2]['embedUrl']


        params_token_w_rls = json.dumps({
            "accessLevel": "View",
            "allowSaveAs": "false",
            "identities": [{
                "username": request.user.username,
                "roles": ["Tenant"],
                "datasets": ["fb0a9b7d-8a62-463e-afc0-a7b31881e0da"]
            }]
        })

        url = 'https://api.powerbi.com/v1.0/myorg/groups/40ee1d8c-8af4-4959-8691-69b99a1d7a9b/reports/aa2baec1-04c7-42b2-b62a-f7e7a8760629/generatetoken'

        #token_w_rls = generate_token_without_rls('https://api.powerbi.com/v1.0/myorg/groups/'+ group_id +'/reports/'+ report_id +'/generatetoken', params_token_w_rls, {'Authorization': 'Bearer ' + token['access_token']})

        token_w_rls = generate_token_without_rls(url, params_token_w_rls, {"Authorization": "Bearer " + token["access_token"], 
  'Content-Type': 'application/json'})

        print(token_w_rls)

        context = {
            'EMBED_ACCESS_TOKEN': token_w_rls['token'],
            'EMBED_URL': report_embed_url,
            'REPORT_ID': report_id
        }

        return HttpResponse(template.render(context, request))
    except IntegrityError:
        return HttpResponse("ERROR: already exists!")


def login_submit_ajax(request):
    # Code here the logging logic 
    username = request.POST.get('username')
    password = request.POST.get('password')

    user = authenticate(username = username, password = password)
    print(user)
    if user is not None:
        login(request,user)
        #return HttpResponse(visor(request))
        #visor(request)
        return HttpResponse('1')
    else:
        return HttpResponse('0')


def generate_request(url, _type, params, headers):
    if _type == "get":
        response = requests.get(url, data=params, headers=headers)
    elif _type == "post":
        response = requests.post(url, data=params, headers=headers)

    print(response.status_code)
    print(response.text)

    if response.status_code == 200:
        return response.json()


def get_api_object(url, _type, params={}, headers={}):
    response = generate_request(url, _type, params, headers)
    if response:
       #_object = response.get('results')[0]
       return response

    return {}

def get_token():
	params = {
		'client_id':'6739ccf1-c8b2-4665-8aac-6ba69e0d23cb',
		'client_secret':'+M7WEm5qpgIk0v0tGpY91Ukga4+v+nku8eNXCkLFFH4=',
		'grant_type':'password',
		'resource':'https://analysis.windows.net/powerbi/api',
		'scope':'openid',
		'username':'pbitest@gestelperu.com',
		'password':'T3csup4326'
	}

	response = get_api_object('https://login.microsoftonline.com/common/oauth2/token', 'post', params)

	return response

def get_groups(params, headers):
	return get_api_object('https://api.powerbi.com/v1.0/myorg/groups', 'get', params, headers)

def get_report(params, headers):
	return get_api_object('https://api.powerbi.com/v1.0/myorg/groups/40ee1d8c-8af4-4959-8691-69b99a1d7a9b/reports', 'get', params, headers)

def generate_token_without_rls(url, params, headers):
	return get_api_object(url, 'post', params, headers)